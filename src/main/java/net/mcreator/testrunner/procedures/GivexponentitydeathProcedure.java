package net.mcreator.testrunner.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector2f;
import net.minecraft.entity.Entity;
import net.minecraft.command.ICommandSource;
import net.minecraft.command.CommandSource;

import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class GivexponentitydeathProcedure extends BcxpreduxModElements.ModElement {
	public GivexponentitydeathProcedure(BcxpreduxModElements instance) {
		super(instance, 208);
		MinecraftForge.EVENT_BUS.register(this);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency entity for procedure Givexponentitydeath!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency x for procedure Givexponentitydeath!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency y for procedure Givexponentitydeath!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency z for procedure Givexponentitydeath!");
			return;
		}
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Givexponentitydeath!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		IWorld world = (IWorld) dependencies.get("world");
		double looptimes = 0;
		double daminuse = 0;
		if (((BcxpreduxModVariables.MapVariables.get(world).usebcxpsys) == (true))) {
			for (int index0 = 0; index0 < (int) ((entity.getPersistentData().getDouble("tagAmount"))); index0++) {
				looptimes = (double) ((looptimes) + 1);
				if (((BcxpreduxModVariables.MapVariables.get(world).bcxpusemultiply) == (true))) {
					if ((Math.round(((entity.getPersistentData().getDouble((("tagDamage") + "" + ((looptimes)))))
							* (BcxpreduxModVariables.MapVariables.get(world).bcxpinteger))) < 1)) {
						daminuse = (double) 1;
					} else {
						daminuse = (double) Math.round(((entity.getPersistentData().getDouble((("tagDamage") + "" + ((looptimes)))))
								* (BcxpreduxModVariables.MapVariables.get(world).bcxpinteger)));
					}
					if (world instanceof ServerWorld) {
						((World) world).getServer().getCommandManager().handleCommand(
								new CommandSource(ICommandSource.DUMMY, new Vector3d(x, y, z), Vector2f.ZERO, (ServerWorld) world, 4, "",
										new StringTextComponent(""), ((World) world).getServer(), null).withFeedbackDisabled(),
								(("experience add ") + "" + ((entity.getPersistentData().getString((("tagName") + "" + ((looptimes)))))) + "" + (" ")
										+ "" + ((new java.text.DecimalFormat("#####").format((daminuse))))));
					}
				} else if (((BcxpreduxModVariables.MapVariables.get(world).bcxpusedivide) == (true))) {
					if ((Math.round(((entity.getPersistentData().getDouble((("tagDamage") + "" + ((looptimes)))))
							/ (BcxpreduxModVariables.MapVariables.get(world).bcxpinteger))) < 1)) {
						daminuse = (double) 1;
					} else {
						daminuse = (double) Math.round(((entity.getPersistentData().getDouble((("tagDamage") + "" + ((looptimes)))))
								/ (BcxpreduxModVariables.MapVariables.get(world).bcxpinteger)));
					}
					if (world instanceof ServerWorld) {
						((World) world).getServer().getCommandManager().handleCommand(
								new CommandSource(ICommandSource.DUMMY, new Vector3d(x, y, z), Vector2f.ZERO, (ServerWorld) world, 4, "",
										new StringTextComponent(""), ((World) world).getServer(), null).withFeedbackDisabled(),
								(("experience add ") + "" + ((entity.getPersistentData().getString((("tagName") + "" + ((looptimes)))))) + "" + (" ")
										+ "" + ((new java.text.DecimalFormat("#####").format((daminuse))))));
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onEntityDeath(LivingDeathEvent event) {
		if (event != null && event.getEntity() != null) {
			Entity entity = event.getEntity();
			Entity sourceentity = event.getSource().getTrueSource();
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			World world = entity.world;
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("sourceentity", sourceentity);
			dependencies.put("event", event);
			this.executeProcedure(dependencies);
		}
	}
}
