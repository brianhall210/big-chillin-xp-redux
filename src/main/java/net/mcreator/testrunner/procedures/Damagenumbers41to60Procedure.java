package net.mcreator.testrunner.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.testrunner.particle.Damagenumber60Particle;
import net.mcreator.testrunner.particle.Damagenumber59Particle;
import net.mcreator.testrunner.particle.Damagenumber58Particle;
import net.mcreator.testrunner.particle.Damagenumber57Particle;
import net.mcreator.testrunner.particle.Damagenumber56Particle;
import net.mcreator.testrunner.particle.Damagenumber55Particle;
import net.mcreator.testrunner.particle.Damagenumber54Particle;
import net.mcreator.testrunner.particle.Damagenumber53Particle;
import net.mcreator.testrunner.particle.Damagenumber52Particle;
import net.mcreator.testrunner.particle.Damagenumber51Particle;
import net.mcreator.testrunner.particle.Damagenumber50Particle;
import net.mcreator.testrunner.particle.Damagenumber49Particle;
import net.mcreator.testrunner.particle.Damagenumber48Particle;
import net.mcreator.testrunner.particle.Damagenumber47Particle;
import net.mcreator.testrunner.particle.Damagenumber46Particle;
import net.mcreator.testrunner.particle.Damagenumber45Particle;
import net.mcreator.testrunner.particle.Damagenumber44Particle;
import net.mcreator.testrunner.particle.Damagenumber43Particle;
import net.mcreator.testrunner.particle.Damagenumber42Particle;
import net.mcreator.testrunner.particle.Damagenumber41Particle;
import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class Damagenumbers41to60Procedure extends BcxpreduxModElements.ModElement {
	public Damagenumbers41to60Procedure(BcxpreduxModElements instance) {
		super(instance, 246);
		MinecraftForge.EVENT_BUS.register(this);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency entity for procedure Damagenumbers41to60!");
			return;
		}
		if (dependencies.get("amount") == null) {
			if (!dependencies.containsKey("amount"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency amount for procedure Damagenumbers41to60!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency x for procedure Damagenumbers41to60!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency y for procedure Damagenumbers41to60!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency z for procedure Damagenumbers41to60!");
			return;
		}
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Damagenumbers41to60!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		double amount = dependencies.get("amount") instanceof Integer ? (int) dependencies.get("amount") : (double) dependencies.get("amount");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		IWorld world = (IWorld) dependencies.get("world");
		double damageamount = 0;
		if (((BcxpreduxModVariables.MapVariables.get(world).showdmgnum) == (true))) {
			if ((Math.ceil((amount)) > ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1))) {
				damageamount = (double) Math.ceil(((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
			} else {
				damageamount = (double) Math.ceil((amount));
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(41))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber41Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(42))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber42Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(43))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber43Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(44))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber44Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(45))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber45Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(46))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber46Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(47))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber47Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(48))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber48Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(49))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber49Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(50))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber50Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(51))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber51Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(52))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber52Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(53))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber53Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(54))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber54Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(55))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber55Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(56))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber56Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(57))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber57Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(58))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber58Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(59))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber59Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(60))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber60Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
		}
	}

	@SubscribeEvent
	public void onEntityAttacked(LivingHurtEvent event) {
		if (event != null && event.getEntity() != null) {
			Entity entity = event.getEntity();
			Entity sourceentity = event.getSource().getTrueSource();
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			double amount = event.getAmount();
			World world = entity.world;
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("amount", amount);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("sourceentity", sourceentity);
			dependencies.put("event", event);
			this.executeProcedure(dependencies);
		}
	}
}
