package net.mcreator.testrunner.procedures;

import net.minecraft.world.IWorld;

import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class TogglexpsystemProcedure extends BcxpreduxModElements.ModElement {
	public TogglexpsystemProcedure(BcxpreduxModElements instance) {
		super(instance, 242);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Togglexpsystem!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		if (((BcxpreduxModVariables.MapVariables.get(world).usebcxpsys) == (true))) {
			BcxpreduxModVariables.MapVariables.get(world).usebcxpsys = (boolean) (false);
			BcxpreduxModVariables.MapVariables.get(world).syncData(world);
			{
				Map<String, Object> $_dependencies = new HashMap<>();
				$_dependencies.put("world", world);
				CheckxpsystemtoggleProcedure.executeProcedure($_dependencies);
			}
			if (((BcxpreduxModVariables.MapVariables.get(world).showxpnum) == (true))) {
				BcxpreduxModVariables.MapVariables.get(world).showxpnum = (boolean) (false);
				BcxpreduxModVariables.MapVariables.get(world).syncData(world);
				{
					Map<String, Object> $_dependencies = new HashMap<>();
					$_dependencies.put("world", world);
					CheckxpnumtoggleProcedure.executeProcedure($_dependencies);
				}
			}
		} else if (((BcxpreduxModVariables.MapVariables.get(world).usebcxpsys) == (false))) {
			BcxpreduxModVariables.MapVariables.get(world).usebcxpsys = (boolean) (true);
			BcxpreduxModVariables.MapVariables.get(world).syncData(world);
			{
				Map<String, Object> $_dependencies = new HashMap<>();
				$_dependencies.put("world", world);
				CheckxpsystemtoggleProcedure.executeProcedure($_dependencies);
			}
		}
	}
}
