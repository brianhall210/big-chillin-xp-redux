package net.mcreator.testrunner.procedures;

import net.minecraftforge.fml.server.ServerLifecycleHooks;

import net.minecraft.world.IWorld;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.ChatType;
import net.minecraft.util.Util;
import net.minecraft.server.MinecraftServer;

import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class TogglexpnumProcedure extends BcxpreduxModElements.ModElement {
	public TogglexpnumProcedure(BcxpreduxModElements instance) {
		super(instance, 236);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Togglexpnum!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		if (((BcxpreduxModVariables.MapVariables.get(world).usebcxpsys) == (true))) {
			if (((BcxpreduxModVariables.MapVariables.get(world).showxpnum) == (true))) {
				BcxpreduxModVariables.MapVariables.get(world).showxpnum = (boolean) (false);
				BcxpreduxModVariables.MapVariables.get(world).syncData(world);
				{
					Map<String, Object> $_dependencies = new HashMap<>();
					$_dependencies.put("world", world);
					CheckxpnumtoggleProcedure.executeProcedure($_dependencies);
				}
			} else if (((BcxpreduxModVariables.MapVariables.get(world).showxpnum) == (false))) {
				BcxpreduxModVariables.MapVariables.get(world).showxpnum = (boolean) (true);
				BcxpreduxModVariables.MapVariables.get(world).syncData(world);
				{
					Map<String, Object> $_dependencies = new HashMap<>();
					$_dependencies.put("world", world);
					CheckxpnumtoggleProcedure.executeProcedure($_dependencies);
				}
			}
		} else {
			if (!world.isRemote()) {
				MinecraftServer mcserv = ServerLifecycleHooks.getCurrentServer();
				if (mcserv != null)
					mcserv.getPlayerList().func_232641_a_(
							new StringTextComponent("\u00A7c\u00A7oCannot activate Floating XP Numbers because bcxptogglexpsys is Off!"),
							ChatType.SYSTEM, Util.DUMMY_UUID);
			}
		}
	}
}
