package net.mcreator.testrunner.procedures;

import net.minecraft.world.IWorld;

import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class ToggledmgnumProcedure extends BcxpreduxModElements.ModElement {
	public ToggledmgnumProcedure(BcxpreduxModElements instance) {
		super(instance, 237);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Toggledmgnum!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		if (((BcxpreduxModVariables.MapVariables.get(world).showdmgnum) == (true))) {
			BcxpreduxModVariables.MapVariables.get(world).showdmgnum = (boolean) (false);
			BcxpreduxModVariables.MapVariables.get(world).syncData(world);
			{
				Map<String, Object> $_dependencies = new HashMap<>();
				$_dependencies.put("world", world);
				CheckdmgnumtoggleProcedure.executeProcedure($_dependencies);
			}
		} else if (((BcxpreduxModVariables.MapVariables.get(world).showdmgnum) == (false))) {
			BcxpreduxModVariables.MapVariables.get(world).showdmgnum = (boolean) (true);
			BcxpreduxModVariables.MapVariables.get(world).syncData(world);
			{
				Map<String, Object> $_dependencies = new HashMap<>();
				$_dependencies.put("world", world);
				CheckdmgnumtoggleProcedure.executeProcedure($_dependencies);
			}
		}
	}
}
