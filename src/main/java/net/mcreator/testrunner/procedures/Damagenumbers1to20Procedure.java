package net.mcreator.testrunner.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.testrunner.particle.Damagenumber9Particle;
import net.mcreator.testrunner.particle.Damagenumber8Particle;
import net.mcreator.testrunner.particle.Damagenumber7Particle;
import net.mcreator.testrunner.particle.Damagenumber6Particle;
import net.mcreator.testrunner.particle.Damagenumber5Particle;
import net.mcreator.testrunner.particle.Damagenumber4Particle;
import net.mcreator.testrunner.particle.Damagenumber3Particle;
import net.mcreator.testrunner.particle.Damagenumber2Particle;
import net.mcreator.testrunner.particle.Damagenumber20Particle;
import net.mcreator.testrunner.particle.Damagenumber1Particle;
import net.mcreator.testrunner.particle.Damagenumber19Particle;
import net.mcreator.testrunner.particle.Damagenumber18Particle;
import net.mcreator.testrunner.particle.Damagenumber17Particle;
import net.mcreator.testrunner.particle.Damagenumber16Particle;
import net.mcreator.testrunner.particle.Damagenumber15Particle;
import net.mcreator.testrunner.particle.Damagenumber14Particle;
import net.mcreator.testrunner.particle.Damagenumber13Particle;
import net.mcreator.testrunner.particle.Damagenumber12Particle;
import net.mcreator.testrunner.particle.Damagenumber11Particle;
import net.mcreator.testrunner.particle.Damagenumber10Particle;
import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class Damagenumbers1to20Procedure extends BcxpreduxModElements.ModElement {
	public Damagenumbers1to20Procedure(BcxpreduxModElements instance) {
		super(instance, 234);
		MinecraftForge.EVENT_BUS.register(this);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency entity for procedure Damagenumbers1to20!");
			return;
		}
		if (dependencies.get("amount") == null) {
			if (!dependencies.containsKey("amount"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency amount for procedure Damagenumbers1to20!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency x for procedure Damagenumbers1to20!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency y for procedure Damagenumbers1to20!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency z for procedure Damagenumbers1to20!");
			return;
		}
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Damagenumbers1to20!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		double amount = dependencies.get("amount") instanceof Integer ? (int) dependencies.get("amount") : (double) dependencies.get("amount");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		IWorld world = (IWorld) dependencies.get("world");
		double damageamount = 0;
		if (((BcxpreduxModVariables.MapVariables.get(world).showdmgnum) == (true))) {
			if ((Math.ceil((amount)) > ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1))) {
				damageamount = (double) Math.ceil(((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
			} else {
				damageamount = (double) Math.ceil((amount));
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(1))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber1Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(2))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber2Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(3))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber3Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(4))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber4Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(5))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber5Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(6))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber6Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(7))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber7Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(8))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber8Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(9))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber9Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(10))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber10Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(11))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber11Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(12))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber12Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(13))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber13Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(14))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber14Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(15))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber15Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(16))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber16Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(17))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber17Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(18))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber18Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(19))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber19Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(20))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber20Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
		}
	}

	@SubscribeEvent
	public void onEntityAttacked(LivingHurtEvent event) {
		if (event != null && event.getEntity() != null) {
			Entity entity = event.getEntity();
			Entity sourceentity = event.getSource().getTrueSource();
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			double amount = event.getAmount();
			World world = entity.world;
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("amount", amount);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("sourceentity", sourceentity);
			dependencies.put("event", event);
			this.executeProcedure(dependencies);
		}
	}
}
