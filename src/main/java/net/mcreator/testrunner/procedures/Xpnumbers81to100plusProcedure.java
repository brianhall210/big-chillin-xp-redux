package net.mcreator.testrunner.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.entity.Entity;

import net.mcreator.testrunner.particle.Xpnumber99Particle;
import net.mcreator.testrunner.particle.Xpnumber98Particle;
import net.mcreator.testrunner.particle.Xpnumber97Particle;
import net.mcreator.testrunner.particle.Xpnumber96Particle;
import net.mcreator.testrunner.particle.Xpnumber95Particle;
import net.mcreator.testrunner.particle.Xpnumber94Particle;
import net.mcreator.testrunner.particle.Xpnumber93Particle;
import net.mcreator.testrunner.particle.Xpnumber92Particle;
import net.mcreator.testrunner.particle.Xpnumber91Particle;
import net.mcreator.testrunner.particle.Xpnumber90Particle;
import net.mcreator.testrunner.particle.Xpnumber89Particle;
import net.mcreator.testrunner.particle.Xpnumber88Particle;
import net.mcreator.testrunner.particle.Xpnumber87Particle;
import net.mcreator.testrunner.particle.Xpnumber86Particle;
import net.mcreator.testrunner.particle.Xpnumber85Particle;
import net.mcreator.testrunner.particle.Xpnumber84Particle;
import net.mcreator.testrunner.particle.Xpnumber83Particle;
import net.mcreator.testrunner.particle.Xpnumber82Particle;
import net.mcreator.testrunner.particle.Xpnumber81Particle;
import net.mcreator.testrunner.particle.Xpnumber100plusParticle;
import net.mcreator.testrunner.particle.Damagenumber1Particle;
import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class Xpnumbers81to100plusProcedure extends BcxpreduxModElements.ModElement {
	public Xpnumbers81to100plusProcedure(BcxpreduxModElements instance) {
		super(instance, 252);
		MinecraftForge.EVENT_BUS.register(this);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency entity for procedure Xpnumbers81to100plus!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency x for procedure Xpnumbers81to100plus!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency y for procedure Xpnumbers81to100plus!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency z for procedure Xpnumbers81to100plus!");
			return;
		}
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Xpnumbers81to100plus!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		IWorld world = (IWorld) dependencies.get("world");
		double loopnum = 0;
		double xpamount = 0;
		if (((BcxpreduxModVariables.MapVariables.get(world).showxpnum) == (true))) {
			for (int index0 = 0; index0 < (int) ((entity.getPersistentData().getDouble("tagAmount"))); index0++) {
				loopnum = (double) ((loopnum) + 1);
				if (((BcxpreduxModVariables.MapVariables.get(world).bcxpusemultiply) == (true))) {
					xpamount = (double) Math.ceil(((entity.getPersistentData().getDouble((("tagDamage") + "" + ((loopnum)))))
							* (BcxpreduxModVariables.MapVariables.get(world).bcxpinteger)));
				} else if (((BcxpreduxModVariables.MapVariables.get(world).bcxpusedivide) == (true))) {
					xpamount = (double) Math.ceil(((entity.getPersistentData().getDouble((("tagDamage") + "" + ((loopnum)))))
							/ (BcxpreduxModVariables.MapVariables.get(world).bcxpinteger)));
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(81))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber81Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(82))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber82Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(83))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber83Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(84))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber84Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(85))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber85Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(86))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber86Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(87))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber87Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(88))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber88Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(89))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber89Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(90))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber90Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(91))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber91Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(92))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber92Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(93))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber93Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(94))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber94Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(95))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber95Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(96))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber96Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(97))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber97Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(98))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber98Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(99))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber99Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(100))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Damagenumber1Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if (((xpamount) >= 101)) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber100plusParticle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onEntityDeath(LivingDeathEvent event) {
		if (event != null && event.getEntity() != null) {
			Entity entity = event.getEntity();
			Entity sourceentity = event.getSource().getTrueSource();
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			World world = entity.world;
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("sourceentity", sourceentity);
			dependencies.put("event", event);
			this.executeProcedure(dependencies);
		}
	}
}
