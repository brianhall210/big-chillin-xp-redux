package net.mcreator.testrunner.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.entity.Entity;

import net.mcreator.testrunner.particle.Xpnumber9Particle;
import net.mcreator.testrunner.particle.Xpnumber8Particle;
import net.mcreator.testrunner.particle.Xpnumber7Particle;
import net.mcreator.testrunner.particle.Xpnumber6Particle;
import net.mcreator.testrunner.particle.Xpnumber5Particle;
import net.mcreator.testrunner.particle.Xpnumber4Particle;
import net.mcreator.testrunner.particle.Xpnumber3Particle;
import net.mcreator.testrunner.particle.Xpnumber2Particle;
import net.mcreator.testrunner.particle.Xpnumber20Particle;
import net.mcreator.testrunner.particle.Xpnumber1Particle;
import net.mcreator.testrunner.particle.Xpnumber19Particle;
import net.mcreator.testrunner.particle.Xpnumber18Particle;
import net.mcreator.testrunner.particle.Xpnumber17Particle;
import net.mcreator.testrunner.particle.Xpnumber16Particle;
import net.mcreator.testrunner.particle.Xpnumber15Particle;
import net.mcreator.testrunner.particle.Xpnumber14Particle;
import net.mcreator.testrunner.particle.Xpnumber13Particle;
import net.mcreator.testrunner.particle.Xpnumber12Particle;
import net.mcreator.testrunner.particle.Xpnumber11Particle;
import net.mcreator.testrunner.particle.Xpnumber10Particle;
import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class Xpnumbers1to20Procedure extends BcxpreduxModElements.ModElement {
	public Xpnumbers1to20Procedure(BcxpreduxModElements instance) {
		super(instance, 235);
		MinecraftForge.EVENT_BUS.register(this);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency entity for procedure Xpnumbers1to20!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency x for procedure Xpnumbers1to20!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency y for procedure Xpnumbers1to20!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency z for procedure Xpnumbers1to20!");
			return;
		}
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Xpnumbers1to20!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		IWorld world = (IWorld) dependencies.get("world");
		double loopnum = 0;
		double xpamount = 0;
		if (((BcxpreduxModVariables.MapVariables.get(world).showxpnum) == (true))) {
			for (int index0 = 0; index0 < (int) ((entity.getPersistentData().getDouble("tagAmount"))); index0++) {
				loopnum = (double) ((loopnum) + 1);
				if (((BcxpreduxModVariables.MapVariables.get(world).bcxpusemultiply) == (true))) {
					xpamount = (double) Math.ceil(((entity.getPersistentData().getDouble((("tagDamage") + "" + ((loopnum)))))
							* (BcxpreduxModVariables.MapVariables.get(world).bcxpinteger)));
				} else if (((BcxpreduxModVariables.MapVariables.get(world).bcxpusedivide) == (true))) {
					xpamount = (double) Math.ceil(((entity.getPersistentData().getDouble((("tagDamage") + "" + ((loopnum)))))
							/ (BcxpreduxModVariables.MapVariables.get(world).bcxpinteger)));
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(1))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber1Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(2))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber2Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(3))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber3Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(4))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber4Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(5))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber5Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(6))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber6Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(7))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber7Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(8))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber8Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(9))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber9Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(10))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber10Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(11))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber11Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(12))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber12Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(13))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber13Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(14))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber14Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(15))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber15Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(16))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber16Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(17))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber17Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(18))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber18Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(19))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber19Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(20))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber20Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onEntityDeath(LivingDeathEvent event) {
		if (event != null && event.getEntity() != null) {
			Entity entity = event.getEntity();
			Entity sourceentity = event.getSource().getTrueSource();
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			World world = entity.world;
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("sourceentity", sourceentity);
			dependencies.put("event", event);
			this.executeProcedure(dependencies);
		}
	}
}
