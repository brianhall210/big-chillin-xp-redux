package net.mcreator.testrunner.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class OnplayerattackProcedure extends BcxpreduxModElements.ModElement {
	public OnplayerattackProcedure(BcxpreduxModElements instance) {
		super(instance, 1);
		MinecraftForge.EVENT_BUS.register(this);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency entity for procedure Onplayerattack!");
			return;
		}
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency sourceentity for procedure Onplayerattack!");
			return;
		}
		if (dependencies.get("amount") == null) {
			if (!dependencies.containsKey("amount"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency amount for procedure Onplayerattack!");
			return;
		}
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Onplayerattack!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		double amount = dependencies.get("amount") instanceof Integer ? (int) dependencies.get("amount") : (double) dependencies.get("amount");
		IWorld world = (IWorld) dependencies.get("world");
		double loopcount = 0;
		double truedamage = 0;
		if (((BcxpreduxModVariables.MapVariables.get(world).usebcxpsys) == (true))) {
			if ((Math.round((amount)) > ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1))) {
				truedamage = (double) ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1);
			} else {
				truedamage = (double) Math.round((amount));
			}
			if ((sourceentity instanceof PlayerEntity)) {
				if (((entity.getPersistentData().getString("playerTags")).contains((sourceentity.getDisplayName().getString())))) {
					for (int index0 = 0; index0 < (int) ((entity.getPersistentData().getDouble("tagAmount"))); index0++) {
						loopcount = (double) ((loopcount) + 1);
						if ((((sourceentity.getDisplayName().getString()))
								.equals((entity.getPersistentData().getString((("tagName") + "" + ((loopcount)))))))) {
							entity.getPersistentData().putDouble((("tagDamage") + "" + ((loopcount))),
									((entity.getPersistentData().getDouble((("tagDamage") + "" + ((loopcount))))) + (truedamage)));
						}
					}
				} else {
					entity.getPersistentData().putString("playerTags", (((entity.getPersistentData().getString("playerTags"))) + ""
							+ ((sourceentity.getDisplayName().getString())) + "" + (" ")));
					entity.getPersistentData().putDouble("tagAmount", ((entity.getPersistentData().getDouble("tagAmount")) + 1));
					entity.getPersistentData().putString((("tagName") + "" + ((entity.getPersistentData().getDouble("tagAmount")))),
							(sourceentity.getDisplayName().getString()));
					entity.getPersistentData().putDouble((("tagDamage") + "" + ((entity.getPersistentData().getDouble("tagAmount")))), (truedamage));
				}
			}
		}
	}

	@SubscribeEvent
	public void onEntityAttacked(LivingHurtEvent event) {
		if (event != null && event.getEntity() != null) {
			Entity entity = event.getEntity();
			Entity sourceentity = event.getSource().getTrueSource();
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			double amount = event.getAmount();
			World world = entity.world;
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("amount", amount);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("sourceentity", sourceentity);
			dependencies.put("event", event);
			this.executeProcedure(dependencies);
		}
	}
}
