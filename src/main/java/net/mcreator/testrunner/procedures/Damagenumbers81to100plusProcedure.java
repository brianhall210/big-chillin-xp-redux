package net.mcreator.testrunner.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.testrunner.particle.Damagenumber99Particle;
import net.mcreator.testrunner.particle.Damagenumber98Particle;
import net.mcreator.testrunner.particle.Damagenumber97Particle;
import net.mcreator.testrunner.particle.Damagenumber96Particle;
import net.mcreator.testrunner.particle.Damagenumber95Particle;
import net.mcreator.testrunner.particle.Damagenumber94Particle;
import net.mcreator.testrunner.particle.Damagenumber93Particle;
import net.mcreator.testrunner.particle.Damagenumber92Particle;
import net.mcreator.testrunner.particle.Damagenumber91Particle;
import net.mcreator.testrunner.particle.Damagenumber90Particle;
import net.mcreator.testrunner.particle.Damagenumber89Particle;
import net.mcreator.testrunner.particle.Damagenumber88Particle;
import net.mcreator.testrunner.particle.Damagenumber87Particle;
import net.mcreator.testrunner.particle.Damagenumber86Particle;
import net.mcreator.testrunner.particle.Damagenumber85Particle;
import net.mcreator.testrunner.particle.Damagenumber84Particle;
import net.mcreator.testrunner.particle.Damagenumber83Particle;
import net.mcreator.testrunner.particle.Damagenumber82Particle;
import net.mcreator.testrunner.particle.Damagenumber81Particle;
import net.mcreator.testrunner.particle.Damagenumber100plusParticle;
import net.mcreator.testrunner.particle.Damagenumber100Particle;
import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class Damagenumbers81to100plusProcedure extends BcxpreduxModElements.ModElement {
	public Damagenumbers81to100plusProcedure(BcxpreduxModElements instance) {
		super(instance, 248);
		MinecraftForge.EVENT_BUS.register(this);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency entity for procedure Damagenumbers81to100plus!");
			return;
		}
		if (dependencies.get("amount") == null) {
			if (!dependencies.containsKey("amount"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency amount for procedure Damagenumbers81to100plus!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency x for procedure Damagenumbers81to100plus!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency y for procedure Damagenumbers81to100plus!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency z for procedure Damagenumbers81to100plus!");
			return;
		}
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Damagenumbers81to100plus!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		double amount = dependencies.get("amount") instanceof Integer ? (int) dependencies.get("amount") : (double) dependencies.get("amount");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		IWorld world = (IWorld) dependencies.get("world");
		double damageamount = 0;
		if (((BcxpreduxModVariables.MapVariables.get(world).showdmgnum) == (true))) {
			if ((Math.ceil((amount)) > ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1))) {
				damageamount = (double) Math.ceil(((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
			} else {
				damageamount = (double) Math.ceil((amount));
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(81))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber81Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(82))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber82Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(83))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber83Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(84))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber84Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(85))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber85Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(86))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber86Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(87))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber87Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(88))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber88Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(89))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber89Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(90))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber90Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(91))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber91Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(92))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber92Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(93))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber93Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(94))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber94Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(95))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber95Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(96))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber96Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(97))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber97Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(98))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber98Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(99))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber99Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(100))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber100Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if (((damageamount) >= 101)) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber100plusParticle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
		}
	}

	@SubscribeEvent
	public void onEntityAttacked(LivingHurtEvent event) {
		if (event != null && event.getEntity() != null) {
			Entity entity = event.getEntity();
			Entity sourceentity = event.getSource().getTrueSource();
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			double amount = event.getAmount();
			World world = entity.world;
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("amount", amount);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("sourceentity", sourceentity);
			dependencies.put("event", event);
			this.executeProcedure(dependencies);
		}
	}
}
