package net.mcreator.testrunner.procedures;

import net.minecraft.world.IWorld;

import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class TogglexporbsProcedure extends BcxpreduxModElements.ModElement {
	public TogglexporbsProcedure(BcxpreduxModElements instance) {
		super(instance, 222);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Togglexporbs!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		if (((BcxpreduxModVariables.MapVariables.get(world).bcxpdroporbs) == (true))) {
			BcxpreduxModVariables.MapVariables.get(world).bcxpdroporbs = (boolean) (false);
			BcxpreduxModVariables.MapVariables.get(world).syncData(world);
			{
				Map<String, Object> $_dependencies = new HashMap<>();
				$_dependencies.put("world", world);
				CheckorbstoggleProcedure.executeProcedure($_dependencies);
			}
		} else if (((BcxpreduxModVariables.MapVariables.get(world).bcxpdroporbs) == (false))) {
			BcxpreduxModVariables.MapVariables.get(world).bcxpdroporbs = (boolean) (true);
			BcxpreduxModVariables.MapVariables.get(world).syncData(world);
			{
				Map<String, Object> $_dependencies = new HashMap<>();
				$_dependencies.put("world", world);
				CheckorbstoggleProcedure.executeProcedure($_dependencies);
			}
		}
	}
}
