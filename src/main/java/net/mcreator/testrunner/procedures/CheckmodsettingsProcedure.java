package net.mcreator.testrunner.procedures;

import net.minecraftforge.fml.server.ServerLifecycleHooks;

import net.minecraft.world.IWorld;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.ChatType;
import net.minecraft.util.Util;
import net.minecraft.server.MinecraftServer;

import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class CheckmodsettingsProcedure extends BcxpreduxModElements.ModElement {
	public CheckmodsettingsProcedure(BcxpreduxModElements instance) {
		super(instance, 232);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Checkmodsettings!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		if (!world.isRemote()) {
			MinecraftServer mcserv = ServerLifecycleHooks.getCurrentServer();
			if (mcserv != null)
				mcserv.getPlayerList().func_232641_a_(
						new StringTextComponent("\u00A71---------------\u00A77\u00A7lBCXP Current Settings\u00A7r\u00A71---------------"),
						ChatType.SYSTEM, Util.DUMMY_UUID);
		}
		{
			Map<String, Object> $_dependencies = new HashMap<>();
			$_dependencies.put("world", world);
			CheckxpsystemtoggleProcedure.executeProcedure($_dependencies);
		}
		if (((BcxpreduxModVariables.MapVariables.get(world).usebcxpsys) == (true))) {
			{
				Map<String, Object> $_dependencies = new HashMap<>();
				$_dependencies.put("world", world);
				CheckxpmathProcedure.executeProcedure($_dependencies);
			}
			{
				Map<String, Object> $_dependencies = new HashMap<>();
				$_dependencies.put("world", world);
				CheckxpnumtoggleProcedure.executeProcedure($_dependencies);
			}
		}
		{
			Map<String, Object> $_dependencies = new HashMap<>();
			$_dependencies.put("world", world);
			CheckdmgnumtoggleProcedure.executeProcedure($_dependencies);
		}
		{
			Map<String, Object> $_dependencies = new HashMap<>();
			$_dependencies.put("world", world);
			CheckorbstoggleProcedure.executeProcedure($_dependencies);
		}
		{
			Map<String, Object> $_dependencies = new HashMap<>();
			$_dependencies.put("world", world);
			ChecksavelvltoggleProcedure.executeProcedure($_dependencies);
		}
		if (!world.isRemote()) {
			MinecraftServer mcserv = ServerLifecycleHooks.getCurrentServer();
			if (mcserv != null)
				mcserv.getPlayerList().func_232641_a_(new StringTextComponent("\u00A71----------------------------------------------------"),
						ChatType.SYSTEM, Util.DUMMY_UUID);
		}
	}
}
