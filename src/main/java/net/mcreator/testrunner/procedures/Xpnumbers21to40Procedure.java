package net.mcreator.testrunner.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.entity.Entity;

import net.mcreator.testrunner.particle.Xpnumber40Particle;
import net.mcreator.testrunner.particle.Xpnumber39Particle;
import net.mcreator.testrunner.particle.Xpnumber38Particle;
import net.mcreator.testrunner.particle.Xpnumber36Particle;
import net.mcreator.testrunner.particle.Xpnumber35Particle;
import net.mcreator.testrunner.particle.Xpnumber34Particle;
import net.mcreator.testrunner.particle.Xpnumber33Particle;
import net.mcreator.testrunner.particle.Xpnumber32Particle;
import net.mcreator.testrunner.particle.Xpnumber31Particle;
import net.mcreator.testrunner.particle.Xpnumber30Particle;
import net.mcreator.testrunner.particle.Xpnumber29Particle;
import net.mcreator.testrunner.particle.Xpnumber28Particle;
import net.mcreator.testrunner.particle.Xpnumber27Particle;
import net.mcreator.testrunner.particle.Xpnumber26Particle;
import net.mcreator.testrunner.particle.Xpnumber25Particle;
import net.mcreator.testrunner.particle.Xpnumber24Particle;
import net.mcreator.testrunner.particle.Xpnumber23Particle;
import net.mcreator.testrunner.particle.Xpnumber22Particle;
import net.mcreator.testrunner.particle.Xpnumber21Particle;
import net.mcreator.testrunner.particle.Damagenumber1Particle;
import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class Xpnumbers21to40Procedure extends BcxpreduxModElements.ModElement {
	public Xpnumbers21to40Procedure(BcxpreduxModElements instance) {
		super(instance, 249);
		MinecraftForge.EVENT_BUS.register(this);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency entity for procedure Xpnumbers21to40!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency x for procedure Xpnumbers21to40!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency y for procedure Xpnumbers21to40!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency z for procedure Xpnumbers21to40!");
			return;
		}
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Xpnumbers21to40!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		IWorld world = (IWorld) dependencies.get("world");
		double loopnum = 0;
		double xpamount = 0;
		if (((BcxpreduxModVariables.MapVariables.get(world).showxpnum) == (true))) {
			for (int index0 = 0; index0 < (int) ((entity.getPersistentData().getDouble("tagAmount"))); index0++) {
				loopnum = (double) ((loopnum) + 1);
				if (((BcxpreduxModVariables.MapVariables.get(world).bcxpusemultiply) == (true))) {
					xpamount = (double) Math.ceil(((entity.getPersistentData().getDouble((("tagDamage") + "" + ((loopnum)))))
							* (BcxpreduxModVariables.MapVariables.get(world).bcxpinteger)));
				} else if (((BcxpreduxModVariables.MapVariables.get(world).bcxpusedivide) == (true))) {
					xpamount = (double) Math.ceil(((entity.getPersistentData().getDouble((("tagDamage") + "" + ((loopnum)))))
							/ (BcxpreduxModVariables.MapVariables.get(world).bcxpinteger)));
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(21))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber21Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(22))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber22Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(23))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber23Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(24))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber24Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(25))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber25Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(26))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber26Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(27))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber27Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(28))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber28Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(29))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber29Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(30))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber30Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(31))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber31Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(32))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber32Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(33))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber33Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(34))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber34Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(35))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber35Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(36))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber36Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(37))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Damagenumber1Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(38))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber38Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(39))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber39Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(40))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber40Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onEntityDeath(LivingDeathEvent event) {
		if (event != null && event.getEntity() != null) {
			Entity entity = event.getEntity();
			Entity sourceentity = event.getSource().getTrueSource();
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			World world = entity.world;
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("sourceentity", sourceentity);
			dependencies.put("event", event);
			this.executeProcedure(dependencies);
		}
	}
}
