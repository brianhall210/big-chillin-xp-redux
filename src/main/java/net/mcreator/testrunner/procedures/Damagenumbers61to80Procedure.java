package net.mcreator.testrunner.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.testrunner.particle.Damagenumber80Particle;
import net.mcreator.testrunner.particle.Damagenumber79Particle;
import net.mcreator.testrunner.particle.Damagenumber78Particle;
import net.mcreator.testrunner.particle.Damagenumber77Particle;
import net.mcreator.testrunner.particle.Damagenumber76Particle;
import net.mcreator.testrunner.particle.Damagenumber75Particle;
import net.mcreator.testrunner.particle.Damagenumber74Particle;
import net.mcreator.testrunner.particle.Damagenumber73Particle;
import net.mcreator.testrunner.particle.Damagenumber72Particle;
import net.mcreator.testrunner.particle.Damagenumber71Particle;
import net.mcreator.testrunner.particle.Damagenumber70Particle;
import net.mcreator.testrunner.particle.Damagenumber69Particle;
import net.mcreator.testrunner.particle.Damagenumber68Particle;
import net.mcreator.testrunner.particle.Damagenumber67Particle;
import net.mcreator.testrunner.particle.Damagenumber66Particle;
import net.mcreator.testrunner.particle.Damagenumber65Particle;
import net.mcreator.testrunner.particle.Damagenumber64Particle;
import net.mcreator.testrunner.particle.Damagenumber63Particle;
import net.mcreator.testrunner.particle.Damagenumber62Particle;
import net.mcreator.testrunner.particle.Damagenumber61Particle;
import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class Damagenumbers61to80Procedure extends BcxpreduxModElements.ModElement {
	public Damagenumbers61to80Procedure(BcxpreduxModElements instance) {
		super(instance, 247);
		MinecraftForge.EVENT_BUS.register(this);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency entity for procedure Damagenumbers61to80!");
			return;
		}
		if (dependencies.get("amount") == null) {
			if (!dependencies.containsKey("amount"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency amount for procedure Damagenumbers61to80!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency x for procedure Damagenumbers61to80!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency y for procedure Damagenumbers61to80!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency z for procedure Damagenumbers61to80!");
			return;
		}
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Damagenumbers61to80!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		double amount = dependencies.get("amount") instanceof Integer ? (int) dependencies.get("amount") : (double) dependencies.get("amount");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		IWorld world = (IWorld) dependencies.get("world");
		double damageamount = 0;
		if (((BcxpreduxModVariables.MapVariables.get(world).showdmgnum) == (true))) {
			if ((Math.ceil((amount)) > ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1))) {
				damageamount = (double) Math.ceil(((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
			} else {
				damageamount = (double) Math.ceil((amount));
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(61))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber61Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(62))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber62Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(63))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber63Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(64))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber64Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(65))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber65Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(66))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber66Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(67))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber67Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(68))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber68Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(69))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber69Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(70))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber70Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(71))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber71Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(72))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber72Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(73))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber73Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(74))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber74Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(75))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber75Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(76))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber76Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(77))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber77Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(78))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber78Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(79))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber79Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(80))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber80Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
		}
	}

	@SubscribeEvent
	public void onEntityAttacked(LivingHurtEvent event) {
		if (event != null && event.getEntity() != null) {
			Entity entity = event.getEntity();
			Entity sourceentity = event.getSource().getTrueSource();
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			double amount = event.getAmount();
			World world = entity.world;
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("amount", amount);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("sourceentity", sourceentity);
			dependencies.put("event", event);
			this.executeProcedure(dependencies);
		}
	}
}
