package net.mcreator.testrunner.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.testrunner.particle.Damagenumber40Particle;
import net.mcreator.testrunner.particle.Damagenumber39Particle;
import net.mcreator.testrunner.particle.Damagenumber38Particle;
import net.mcreator.testrunner.particle.Damagenumber37Particle;
import net.mcreator.testrunner.particle.Damagenumber36Particle;
import net.mcreator.testrunner.particle.Damagenumber35Particle;
import net.mcreator.testrunner.particle.Damagenumber34Particle;
import net.mcreator.testrunner.particle.Damagenumber33Particle;
import net.mcreator.testrunner.particle.Damagenumber32Particle;
import net.mcreator.testrunner.particle.Damagenumber31Particle;
import net.mcreator.testrunner.particle.Damagenumber29Particle;
import net.mcreator.testrunner.particle.Damagenumber28Particle;
import net.mcreator.testrunner.particle.Damagenumber27Particle;
import net.mcreator.testrunner.particle.Damagenumber26Particle;
import net.mcreator.testrunner.particle.Damagenumber25Particle;
import net.mcreator.testrunner.particle.Damagenumber24Particle;
import net.mcreator.testrunner.particle.Damagenumber23Particle;
import net.mcreator.testrunner.particle.Damagenumber22Particle;
import net.mcreator.testrunner.particle.Damagenumber21Particle;
import net.mcreator.testrunner.particle.Damagenumber1Particle;
import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class Damagenumbers21to40Procedure extends BcxpreduxModElements.ModElement {
	public Damagenumbers21to40Procedure(BcxpreduxModElements instance) {
		super(instance, 245);
		MinecraftForge.EVENT_BUS.register(this);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency entity for procedure Damagenumbers21to40!");
			return;
		}
		if (dependencies.get("amount") == null) {
			if (!dependencies.containsKey("amount"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency amount for procedure Damagenumbers21to40!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency x for procedure Damagenumbers21to40!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency y for procedure Damagenumbers21to40!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency z for procedure Damagenumbers21to40!");
			return;
		}
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Damagenumbers21to40!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		double amount = dependencies.get("amount") instanceof Integer ? (int) dependencies.get("amount") : (double) dependencies.get("amount");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		IWorld world = (IWorld) dependencies.get("world");
		double damageamount = 0;
		if (((BcxpreduxModVariables.MapVariables.get(world).showdmgnum) == (true))) {
			if ((Math.ceil((amount)) > ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1))) {
				damageamount = (double) Math.ceil(((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
			} else {
				damageamount = (double) Math.ceil((amount));
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(21))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber21Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(22))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber22Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(23))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber23Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(24))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber24Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(25))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber25Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(26))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber26Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(27))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber27Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(28))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber28Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(29))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber29Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(30))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber1Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(31))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber31Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(32))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber32Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(33))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber33Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(34))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber34Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(35))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber35Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(36))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber36Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(37))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber37Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(38))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber38Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(39))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber39Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
			if ((((new java.text.DecimalFormat("#####").format((damageamount)))).equals((new java.text.DecimalFormat("#####").format(40))))) {
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(Damagenumber40Particle.particle, x, (y + 1.5), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
				}
			}
		}
	}

	@SubscribeEvent
	public void onEntityAttacked(LivingHurtEvent event) {
		if (event != null && event.getEntity() != null) {
			Entity entity = event.getEntity();
			Entity sourceentity = event.getSource().getTrueSource();
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			double amount = event.getAmount();
			World world = entity.world;
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("amount", amount);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("sourceentity", sourceentity);
			dependencies.put("event", event);
			this.executeProcedure(dependencies);
		}
	}
}
