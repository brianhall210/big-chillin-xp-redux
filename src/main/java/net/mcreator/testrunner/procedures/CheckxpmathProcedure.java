package net.mcreator.testrunner.procedures;

import net.minecraftforge.fml.server.ServerLifecycleHooks;

import net.minecraft.world.IWorld;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.ChatType;
import net.minecraft.util.Util;
import net.minecraft.server.MinecraftServer;

import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;

@BcxpreduxModElements.ModElement.Tag
public class CheckxpmathProcedure extends BcxpreduxModElements.ModElement {
	public CheckxpmathProcedure(BcxpreduxModElements instance) {
		super(instance, 217);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Checkxpmath!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		if (((BcxpreduxModVariables.MapVariables.get(world).bcxpusedivide) == (true))) {
			if (!world.isRemote()) {
				MinecraftServer mcserv = ServerLifecycleHooks.getCurrentServer();
				if (mcserv != null)
					mcserv.getPlayerList().func_232641_a_(
							new StringTextComponent((("\u00A73Global XP Earn Rate: \u00A7fDMG \u00A7cdivided \u00A7fby ") + "" + ("\u00A76") + ""
									+ ((new java.text.DecimalFormat("#####.##")
											.format((BcxpreduxModVariables.MapVariables.get(world).bcxpinteger)))))),
							ChatType.SYSTEM, Util.DUMMY_UUID);
			}
		} else {
			if (!world.isRemote()) {
				MinecraftServer mcserv = ServerLifecycleHooks.getCurrentServer();
				if (mcserv != null)
					mcserv.getPlayerList().func_232641_a_(
							new StringTextComponent((("\u00A73Global XP Earn Rate: \u00A7fDMG \u00A7amultiplied \u00A7fby ") + "" + ("\u00A76") + ""
									+ ((new java.text.DecimalFormat("#####.##")
											.format((BcxpreduxModVariables.MapVariables.get(world).bcxpinteger)))))),
							ChatType.SYSTEM, Util.DUMMY_UUID);
			}
		}
	}
}
