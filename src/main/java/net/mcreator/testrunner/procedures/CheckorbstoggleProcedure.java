package net.mcreator.testrunner.procedures;

import net.minecraftforge.fml.server.ServerLifecycleHooks;

import net.minecraft.world.IWorld;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.ChatType;
import net.minecraft.util.Util;
import net.minecraft.server.MinecraftServer;

import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;

@BcxpreduxModElements.ModElement.Tag
public class CheckorbstoggleProcedure extends BcxpreduxModElements.ModElement {
	public CheckorbstoggleProcedure(BcxpreduxModElements instance) {
		super(instance, 231);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Checkorbstoggle!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		if (((BcxpreduxModVariables.MapVariables.get(world).bcxpdroporbs) == (false))) {
			if (!world.isRemote()) {
				MinecraftServer mcserv = ServerLifecycleHooks.getCurrentServer();
				if (mcserv != null)
					mcserv.getPlayerList().func_232641_a_(new StringTextComponent("\u00A7bSlain entities drop XP Orbs: \u00A7cOff"), ChatType.SYSTEM,
							Util.DUMMY_UUID);
			}
		} else if (((BcxpreduxModVariables.MapVariables.get(world).bcxpdroporbs) == (true))) {
			if (!world.isRemote()) {
				MinecraftServer mcserv = ServerLifecycleHooks.getCurrentServer();
				if (mcserv != null)
					mcserv.getPlayerList().func_232641_a_(new StringTextComponent("\u00A7bSlain entities drop XP Orbs: \u00A7aOn"), ChatType.SYSTEM,
							Util.DUMMY_UUID);
			}
		}
	}
}
