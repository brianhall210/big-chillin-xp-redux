package net.mcreator.testrunner.procedures;

import net.minecraftforge.fml.server.ServerLifecycleHooks;

import net.minecraft.world.IWorld;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.ChatType;
import net.minecraft.util.Util;
import net.minecraft.server.MinecraftServer;

import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;

@BcxpreduxModElements.ModElement.Tag
public class CheckdmgnumtoggleProcedure extends BcxpreduxModElements.ModElement {
	public CheckdmgnumtoggleProcedure(BcxpreduxModElements instance) {
		super(instance, 239);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Checkdmgnumtoggle!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		if (((BcxpreduxModVariables.MapVariables.get(world).showdmgnum) == (true))) {
			if (!world.isRemote()) {
				MinecraftServer mcserv = ServerLifecycleHooks.getCurrentServer();
				if (mcserv != null)
					mcserv.getPlayerList().func_232641_a_(new StringTextComponent("\u00A73Floating Damage Numbers: \u00A7aOn"), ChatType.SYSTEM,
							Util.DUMMY_UUID);
			}
		} else if (((BcxpreduxModVariables.MapVariables.get(world).showdmgnum) == (false))) {
			if (!world.isRemote()) {
				MinecraftServer mcserv = ServerLifecycleHooks.getCurrentServer();
				if (mcserv != null)
					mcserv.getPlayerList().func_232641_a_(new StringTextComponent("\u00A73Floating Damage Numbers: \u00A7cOff"), ChatType.SYSTEM,
							Util.DUMMY_UUID);
			}
		}
	}
}
