package net.mcreator.testrunner.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.entity.Entity;

import net.mcreator.testrunner.particle.Xpnumber80Particle;
import net.mcreator.testrunner.particle.Xpnumber79Particle;
import net.mcreator.testrunner.particle.Xpnumber78Particle;
import net.mcreator.testrunner.particle.Xpnumber76Particle;
import net.mcreator.testrunner.particle.Xpnumber75Particle;
import net.mcreator.testrunner.particle.Xpnumber74Particle;
import net.mcreator.testrunner.particle.Xpnumber73Particle;
import net.mcreator.testrunner.particle.Xpnumber72Particle;
import net.mcreator.testrunner.particle.Xpnumber71Particle;
import net.mcreator.testrunner.particle.Xpnumber70Particle;
import net.mcreator.testrunner.particle.Xpnumber69Particle;
import net.mcreator.testrunner.particle.Xpnumber68Particle;
import net.mcreator.testrunner.particle.Xpnumber67Particle;
import net.mcreator.testrunner.particle.Xpnumber66Particle;
import net.mcreator.testrunner.particle.Xpnumber65Particle;
import net.mcreator.testrunner.particle.Xpnumber64Particle;
import net.mcreator.testrunner.particle.Xpnumber63Particle;
import net.mcreator.testrunner.particle.Xpnumber62Particle;
import net.mcreator.testrunner.particle.Xpnumber61Particle;
import net.mcreator.testrunner.particle.Damagenumber1Particle;
import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class Xpnumbers61to80Procedure extends BcxpreduxModElements.ModElement {
	public Xpnumbers61to80Procedure(BcxpreduxModElements instance) {
		super(instance, 251);
		MinecraftForge.EVENT_BUS.register(this);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency entity for procedure Xpnumbers61to80!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency x for procedure Xpnumbers61to80!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency y for procedure Xpnumbers61to80!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency z for procedure Xpnumbers61to80!");
			return;
		}
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Xpnumbers61to80!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		IWorld world = (IWorld) dependencies.get("world");
		double loopnum = 0;
		double xpamount = 0;
		if (((BcxpreduxModVariables.MapVariables.get(world).showxpnum) == (true))) {
			for (int index0 = 0; index0 < (int) ((entity.getPersistentData().getDouble("tagAmount"))); index0++) {
				loopnum = (double) ((loopnum) + 1);
				if (((BcxpreduxModVariables.MapVariables.get(world).bcxpusemultiply) == (true))) {
					xpamount = (double) Math.ceil(((entity.getPersistentData().getDouble((("tagDamage") + "" + ((loopnum)))))
							* (BcxpreduxModVariables.MapVariables.get(world).bcxpinteger)));
				} else if (((BcxpreduxModVariables.MapVariables.get(world).bcxpusedivide) == (true))) {
					xpamount = (double) Math.ceil(((entity.getPersistentData().getDouble((("tagDamage") + "" + ((loopnum)))))
							/ (BcxpreduxModVariables.MapVariables.get(world).bcxpinteger)));
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(61))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber61Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(62))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber62Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(63))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber63Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(64))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber64Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(65))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber65Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(66))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber66Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(67))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber67Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(68))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber68Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(69))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber69Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(70))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber70Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(71))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber71Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(72))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber72Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(73))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber73Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(74))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber74Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(75))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber75Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(76))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber76Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(77))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Damagenumber1Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(78))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber78Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(79))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber79Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(80))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber80Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onEntityDeath(LivingDeathEvent event) {
		if (event != null && event.getEntity() != null) {
			Entity entity = event.getEntity();
			Entity sourceentity = event.getSource().getTrueSource();
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			World world = entity.world;
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("sourceentity", sourceentity);
			dependencies.put("event", event);
			this.executeProcedure(dependencies);
		}
	}
}
