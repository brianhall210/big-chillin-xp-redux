package net.mcreator.testrunner.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.entity.Entity;

import net.mcreator.testrunner.particle.Xpnumber60Particle;
import net.mcreator.testrunner.particle.Xpnumber59Particle;
import net.mcreator.testrunner.particle.Xpnumber58Particle;
import net.mcreator.testrunner.particle.Xpnumber57Particle;
import net.mcreator.testrunner.particle.Xpnumber56Particle;
import net.mcreator.testrunner.particle.Xpnumber55Particle;
import net.mcreator.testrunner.particle.Xpnumber53Particle;
import net.mcreator.testrunner.particle.Xpnumber52Particle;
import net.mcreator.testrunner.particle.Xpnumber51Particle;
import net.mcreator.testrunner.particle.Xpnumber50Particle;
import net.mcreator.testrunner.particle.Xpnumber49Particle;
import net.mcreator.testrunner.particle.Xpnumber48Particle;
import net.mcreator.testrunner.particle.Xpnumber47Particle;
import net.mcreator.testrunner.particle.Xpnumber46Particle;
import net.mcreator.testrunner.particle.Xpnumber45Particle;
import net.mcreator.testrunner.particle.Xpnumber44Particle;
import net.mcreator.testrunner.particle.Xpnumber43Particle;
import net.mcreator.testrunner.particle.Xpnumber42Particle;
import net.mcreator.testrunner.particle.Xpnumber41Particle;
import net.mcreator.testrunner.particle.Damagenumber1Particle;
import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class Xpnumbers41to60Procedure extends BcxpreduxModElements.ModElement {
	public Xpnumbers41to60Procedure(BcxpreduxModElements instance) {
		super(instance, 250);
		MinecraftForge.EVENT_BUS.register(this);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency entity for procedure Xpnumbers41to60!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency x for procedure Xpnumbers41to60!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency y for procedure Xpnumbers41to60!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency z for procedure Xpnumbers41to60!");
			return;
		}
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Xpnumbers41to60!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		IWorld world = (IWorld) dependencies.get("world");
		double loopnum = 0;
		double xpamount = 0;
		if (((BcxpreduxModVariables.MapVariables.get(world).showxpnum) == (true))) {
			for (int index0 = 0; index0 < (int) ((entity.getPersistentData().getDouble("tagAmount"))); index0++) {
				loopnum = (double) ((loopnum) + 1);
				if (((BcxpreduxModVariables.MapVariables.get(world).bcxpusemultiply) == (true))) {
					xpamount = (double) Math.ceil(((entity.getPersistentData().getDouble((("tagDamage") + "" + ((loopnum)))))
							* (BcxpreduxModVariables.MapVariables.get(world).bcxpinteger)));
				} else if (((BcxpreduxModVariables.MapVariables.get(world).bcxpusedivide) == (true))) {
					xpamount = (double) Math.ceil(((entity.getPersistentData().getDouble((("tagDamage") + "" + ((loopnum)))))
							/ (BcxpreduxModVariables.MapVariables.get(world).bcxpinteger)));
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(41))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber41Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(42))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber42Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(43))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber43Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(44))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber44Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(45))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber45Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(46))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber46Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(47))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber47Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(48))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber48Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(49))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber49Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(50))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber50Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(51))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber51Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(52))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber52Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(53))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber53Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(54))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Damagenumber1Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(55))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber55Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(56))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber56Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(57))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber57Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(58))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber58Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(59))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber59Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
				if ((((new java.text.DecimalFormat("#####").format((xpamount)))).equals((new java.text.DecimalFormat("#####").format(60))))) {
					if (world instanceof ServerWorld) {
						((ServerWorld) world).spawnParticle(Xpnumber60Particle.particle, x, (y + 1.6), z, (int) 1, 0.3, 0.1, 0.3, 0.1);
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onEntityDeath(LivingDeathEvent event) {
		if (event != null && event.getEntity() != null) {
			Entity entity = event.getEntity();
			Entity sourceentity = event.getSource().getTrueSource();
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			World world = entity.world;
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("sourceentity", sourceentity);
			dependencies.put("event", event);
			this.executeProcedure(dependencies);
		}
	}
}
