package net.mcreator.testrunner.procedures;

import net.minecraft.world.IWorld;

import net.mcreator.testrunner.BcxpreduxModVariables;
import net.mcreator.testrunner.BcxpreduxModElements;
import net.mcreator.testrunner.BcxpreduxMod;

import java.util.Map;
import java.util.HashMap;

@BcxpreduxModElements.ModElement.Tag
public class UsexpmultiplyProcedure extends BcxpreduxModElements.ModElement {
	public UsexpmultiplyProcedure(BcxpreduxModElements instance) {
		super(instance, 213);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BcxpreduxMod.LOGGER.warn("Failed to load dependency world for procedure Usexpmultiply!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		if (((BcxpreduxModVariables.MapVariables.get(world).bcxpusemultiply) == (false))) {
			BcxpreduxModVariables.MapVariables.get(world).bcxpusemultiply = (boolean) (true);
			BcxpreduxModVariables.MapVariables.get(world).syncData(world);
			BcxpreduxModVariables.MapVariables.get(world).bcxpusedivide = (boolean) (false);
			BcxpreduxModVariables.MapVariables.get(world).syncData(world);
			{
				Map<String, Object> $_dependencies = new HashMap<>();
				$_dependencies.put("world", world);
				CheckxpmathProcedure.executeProcedure($_dependencies);
			}
		} else {
			{
				Map<String, Object> $_dependencies = new HashMap<>();
				$_dependencies.put("world", world);
				CheckxpmathProcedure.executeProcedure($_dependencies);
			}
		}
	}
}
